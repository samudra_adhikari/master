class objectsandclass:
  def __init__(self, name, age):
    self.name = name
    self.age = age

  def myfunc(self):
    print("Hello my name is " + self.name)

p1 = objectsandclass("Samudra Raj Adhikari", 28)
p1.myfunc()

